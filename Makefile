all: clean build

build:
	boot2docker start
	# docker build -t docker-node-express .
	docker run -p 27017:27017 --name "mongodb" -d mongo
	# docker run -p 8080:8080 --name="docker-node-express" --link mongodb:mongo -d docker-node-express
	# docker run -p 8080:8080 --name="docker-node-express" --link mongodb:mongo -v "${PWD}":/usr/src/app -w /usr/src/app -d node:0.12.0 node main.js
	docker run -p 8080:8080 --name="docker-node-express" --link mongodb:mongo -v "${PWD}":/usr/src/app -w /usr/src/app -d node:0.12.0 node_modules/nodemon/bin/nodemon.js main.js

clean:
	docker stop mongodb &> /dev/null || exit 0
	docker rmi -f mongodb &> /dev/null || exit 0
	docker rm mongodb &> /dev/null || exit 0
	docker stop docker-node-express &> /dev/null || exit 0
	docker rmi -f docker-node-express &> /dev/null || exit 0
	docker rm docker-node-express &> /dev/null || exit 0
