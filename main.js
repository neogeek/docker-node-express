var express = require('express');
var app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://' + process.env.MONGO_PORT_27017_TCP_ADDR + ':' + process.env.MONGO_PORT_27017_TCP_PORT + '/test');

app.use(express.static(__dirname + '/static'));

app.listen(8080);
