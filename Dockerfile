FROM node:0.12.0

COPY . /src

RUN cd /src; npm install

CMD ["node", "/src/main.js"]

EXPOSE  8080
